// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <iterator> // istream_iterator
#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// ----
// read
// ----

TEST(CollatzFixture, read) {
    string s("1 10\n");
    const pair<int, int> p = collatz_read(s);
    ASSERT_EQ(p.first,   1);
    ASSERT_EQ(p.second, 10);
}

// ----
// eval
// ----

TEST(CollatzFixture, eval0) {
    tuple<int, int, int> t = collatz_eval(make_pair(1, 1));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i,  1);
    ASSERT_EQ(j, 1);
    ASSERT_EQ(v, 1 );
}

TEST(CollatzFixture, eval1) {
    tuple<int, int, int> t = collatz_eval(make_pair( 2, 1));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i, 2);
    ASSERT_EQ(j, 1);
    ASSERT_EQ(v, 2);
}

TEST(CollatzFixture, eval2) {
    tuple<int, int, int> t = collatz_eval(make_pair(999999, 999999 ));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i, 999999);
    ASSERT_EQ(j, 999999);
    ASSERT_EQ(v, 259);
}

TEST(CollatzFixture, eval3) {
    tuple<int, int, int> t = collatz_eval(make_pair(743771, 509752 ));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i, 743771);
    ASSERT_EQ(j, 509752);
    ASSERT_EQ(v, 509);
}

TEST(CollatzFixture, eval4) {
    tuple<int, int, int> t = collatz_eval(make_pair(167782, 340657 ));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i, 167782);
    ASSERT_EQ(j, 340657);
    ASSERT_EQ(v, 443);
}


// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream oss;
    collatz_print(oss, make_tuple(1, 10, 20));
    ASSERT_EQ(oss.str(), "1 10 20\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream iss("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream oss;
    collatz_solve(iss, oss);
    //  Which is: "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n"
    ASSERT_EQ("1 10 20\n100 200 125\n201 210 89\n900 1000 174\n", oss.str());
}
